#!/bin/bash

sudo pacman -S \
  zsh \
  nvim \
  tmux \
  xorg-server \
  xorg-xinit \
  xorg-xrandr \
  firefox \
  wezterm
